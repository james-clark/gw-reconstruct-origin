#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Waveforms routines.
"""

from __future__ import (division, print_function, absolute_import)

import numpy as np
from scipy import interpolate

import lal
import lalsimulation as ls

from pycbc.types import FrequencySeries
from .strain import Strain

# Max amplitude orders found in LALSimulation (not accessible from outside of LALSim) */
MAX_NONPRECESSING_AMP_PN_ORDER = 6
MAX_PRECESSING_AMP_PN_ORDER = 3

def flow_to_fmin(flow, amp_order, approx):
    """
    Compute the minimum frequency for waveform generation using amplitude orders
    above Newtonian.  The waveform generator turns on all orders at the orbital
    associated with fMin, so information from higher orders is not included at fLow
    unless fMin is sufficiently low.

    This function replicates conservative choices made in LALInference
    """
    if amp_order == -1:
        if approx == ls.SpinTaylorT2 or approx == ls.SpinTaylorT4:
            amp_order = MAX_PRECESSING_AMP_PN_ORDER
        else:
            amp_order = MAX_NONPRECESSING_AMP_PN_ORDER
    return flow * 2./(amp_order+2)

def timedelay(ifo, ra, dec, time):
    tgps = lal.LIGOTimeGPS(time)

    location = lal.CachedDetectors[det_index(ifo)].location

    return lal.TimeDelayFromEarthCenter(location, ra, dec, tgps)


def timeshift(h, time_shift, sample_rate, duration):
    N = len(h)
    df = 1./duration

    sin_pi_df_t = np.sin(np.pi*df*time_shift)
    cos_pi_df_t = np.cos(np.pi*df*time_shift)
    sin_pi_df_t2 = sin_pi_df_t*sin_pi_df_t
    dshift = -2.0*sin_pi_df_t2 - 2.0*1j*cos_pi_df_t*sin_pi_df_t
    shift = 1.0

    for i in range(N):
        h[i] *= shift
        shift += shift*dshift

    return h


def det_index(ifo):
    if ifo == 'H1':
        diff = lal.LALDetectorIndexLHODIFF
    elif ifo == 'L1':
        diff = lal.LALDetectorIndexLLODIFF
    elif ifo == 'V1':
        diff = lal.LALDetectorIndexVIRGODIFF
    else:
        raise ValueError('detector not recognized: ' + ifo)

    return diff


def sample_to_waveform_params(sample, flow=None):
    params = {}
    "getting params"
    try:
        amp_order = int(sample['lal_amporder'])
    except (ValueError, IndexError, KeyError):
        raise ValueError
        amp_order = -1
    params['amp_order'] = amp_order

    try:
        phase_order = int(sample['lal_pnorder'])
    except (ValueError, IndexError, KeyError):
        raise ValueError
        phase_order = -1
    params['phase_order'] = phase_order

    try:
        approx = int(sample['lal_approximant'])
    except (ValueError, IndexError, KeyError):
        raise ValueError
        approx = ls.IMRPhenomPv2
    params['approx'] = approx

    if flow is None:
        try:
            flow = sample['flow']
        except (ValueError, IndexError, KeyError):
            raise ValueError
            flow = 30.
    params['flow'] = flow

    try:
        fref = sample['f_ref']
    except (ValueError, IndexError, KeyError):
        raise ValueError
        fref = 30.
    params['fref'] = fref

    params['fmin'] = flow_to_fmin(flow, amp_order, approx)

    try:
        dist = sample['distance']
    except (ValueError, IndexError, KeyError):
        dist = np.exp(sample['logdistance'])
    dist *= 1E6 * ls.lal.PC_SI

    q = sample['q']
    try:
        factor = sample['mc'] * np.power(1 + q, 1.0/5.0)
    except (ValueError, IndexError, KeyError):
        factor = sample['chirpmass'] * np.power(1 + q, 1.0/5.0)

    m1 = factor * np.power(q, -3.0/5.0) * lal.MSUN_SI
    m2 = factor * np.power(q, 2.0/5.0) * lal.MSUN_SI

    try:
        q1 = sample['q1']
    except (ValueError, IndexError, KeyError):
        q1 = 0.

    try:
        q2 = sample['q2']
    except (ValueError, IndexError, KeyError):
        q2 = 0.

    ra = sample['ra']
    dec = sample['dec']
    try:
        psi = sample['psi']
    except (ValueError, IndexError, KeyError):
        psi = sample['polarisation']

    # These are identical if non-precessing, in which case this will be overwritten
    iota = np.arccos(sample['costheta_jn'])

    s1x, s1y, s1z = 0., 0., 0.
    s2x, s2y, s2z = 0., 0., 0.

    # Try to treat this as a precessing analysis
    try:
        theta_jn = np.arccos(sample['costheta_jn'])
        phi_jl = sample['phi_jl']
        try:
            tilt1 = sample['tilt1']
            tilt2 = sample['tilt2']
        except (ValueError, IndexError, KeyError):
            raise ValueError
            tilt1 = np.arccos(sample['costilt1'])
            tilt2 = np.arccos(sample['costilt2'])
        phi12 = sample['phi12']
        a1 = sample['a1']
        a2 = sample['a2']

        iota, s1x, s1y, s1z, s2x, s2y, s2z = \
            ls.SimInspiralTransformPrecessingNewInitialConditions(theta_jn, phi_jl, tilt1, tilt2,
                                                                  phi12, a1, a2, m1, m2, fref)

    # try the new (post-processing) convention for spin-aligned models
    except (ValueError, IndexError, KeyError):
        try:
            s1z = sample['a1z']
            s2z = sample['a2z']

        # Maybe it's raw chain file where a1 is the a1z checked for above
        except (ValueError, IndexError, KeyError):
            try:
                s1z = sample['a1']
                s2z = sample['a2']

            # It must be non-spinning
            except (ValueError, IndexError, KeyError):
                pass

    try:
        phase = sample['phi_orb']
    except (ValueError, IndexError, KeyError):
        try:
            phase = sample['phase']
        except (ValueError, IndexError, KeyError):
            phase = sample['phase_maxl']
            print('WARNING: Samples already marginalized over phase.')
            print('         Begrudgingly using max loglikelihood estimates.')

    try:
        time = sample['time']
    except (ValueError, IndexError, KeyError):
        time = sample['time_maxl']
        print('WARNING: Samples already marginalized over time.')
        print('         Begrudgingly using max loglikelihood estimates.')

    # Check for spline calibration model
    try:
        spcal_active = sample['spcal_active']
    except (ValueError, IndexError, KeyError):
        spcal_active = False

    spcal_params = ['spcal_active']
    spcal_vals = [spcal_active]
    if spcal_active:
        sample_params = sample.dtype.names

        ifos = np.unique([param.split('_')[0] for param in sample_params if 'spcal_freq' in param])

        for ifo in ifos:
            # Get frequencies of spline control points
            freq_params = sorted([param for param in sample_params if
                                  '{}_spcal_freq'.format(ifo) in param])
            spcal_params += freq_params
            spcal_vals += [sample[param] for param in freq_params]

            # Amplitude calibration model
            amp_params = sorted([param for param in sample_params if
                                 '{0}_spcal_amp'.format(ifo) in param])
            spcal_params += amp_params
            spcal_vals += [sample[param] for param in amp_params]

            # Phase calibration model
            phase_params = sorted([param for param in sample_params if
                                   '{0}_spcal_phase'.format(ifo) in param])
            spcal_params += phase_params
            spcal_vals += [sample[param] for param in phase_params]

    param_names = ['m1', 'm2', 's1x', 's1y', 's1z', 's2x', 's2y',
                   's2z', 'ra', 'dec', 'time', 'dist', 'iota', 'psi', 'phase',
                   'q1', 'q2']
    param_vals = [m1, m2, s1x, s1y, s1z, s2x, s2y, s2z,
                  ra, dec, time, dist, iota, psi, phase,
                  q1, q2]

    param_names += spcal_params
    param_vals += spcal_vals

    for param, val in zip(param_names, param_vals):
        params[param] = val

    return params




def generate_hphc(m1, m2, dist=1E6*ls.lal.PC_SI, iota=0., s1x=0., s1y=0.,
        s1z=0., s2x=0., s2y=0., s2z=0., phase=0., fmin=30., flow=30., fref=30.,
        fhigh=None, lambda1=0., lambda2=0., spin_order=-1, tidal_order=-1,
        eccentricity_order=-1, dquad_mon1=0, dquad_mon2=0, numrel_data=0,
        modes_choice=0, frame_axis=0, side_band=0, amp_order=-1, phase_order=-1,
        approx=ls.IMRPhenomPv2, sample_rate=16384., duration=8.,
        choose_fd=False):

    lal_pars = lal.CreateDict()
    ls.SimInspiralWaveformParamsInsertPNPhaseOrder(lal_pars,phase_order)
    ls.SimInspiralWaveformParamsInsertPNAmplitudeOrder(lal_pars,amplitude_order)
    ls.SimInspiralWaveformParamsInsertTidalLambda1(lal_pars, lambda1)
    ls.SimInspiralWaveformParamsInsertTidalLambda2(lal_pars, lambda2)
    ls.SimInspiralWaveformParamsInsertPNSpinOrder(lal_pars, spin_order)
    ls.SimInspiralWaveformParamsInsertPNTidalOrder(lal_pars, tidal_order)
    ls.SimInspiralWaveformParamsInsertPNEccentricityOrder(lal_pars, eccentricity_order)
    ls.SimInspiralWaveformParamsInsertdQuadMon1(lal_pars, dquad_mon1)
    ls.SimInspiralWaveformParamsInsertdQuadMon2(lal_pars, dquad_mon2)
    ls.SimInspiralWaveformParamsInsertNumRelData(lal_pars, numrel_data)
    ls.SimInspiralWaveformParamsInsertModesChoice(lal_pars, modes_choice)
    ls.SimInspiralWaveformParamsInsertFrameAxis(lal_pars, frame_axis)
    ls.SimInspiralWaveformParamsInsertSideband(lal_pars, side_band)

    if ls.SimInspiralImplementedFDApproximants(approx):
        if choose_fd:
            df = 1./duration
            fmax = fhigh if fhigh else 0.
            nfreqs = int(duration*sample_rate/2 + 1)

            hplus, hcross = ls.SimInspiralChooseFDWaveform(m1=m1, m2=m2,
                    S1x=s1x, S1y=s1y, S1z=s1z, S2x=s2x, S2y=s2y, S2z=s2z,
                    distance=dist, inclination=iota, phiRef=phase, deltaF=df,
                    f_min=fmin, f_max=fmax, f_ref=fref, LALpars=lal_pars,
                    approximant=approx)

            hpdata = np.zeros(nfreqs, dtype=complex)
            hcdata = np.zeros(nfreqs, dtype=complex)

            start, end = int(hplus.f0/hplus.deltaF), int(hplus.f0/hplus.deltaF) + hplus.data.length
            hpdata[start:end] = hplus.data.data
            hcdata[start:end] = hcross.data.data

            # Cut down to length if necessary
            if hpdata.shape[0] > nfreqs:
                hpdata = hpdata[:nfreqs]
                hcdata = hcdata[:nfreqs]

            # Zero-out anything below the starting frequency, in case fmin < flow
            hpdata[:int(flow/df)] = 0. + 1j*0.
            hcdata[:int(flow/df)] = 0. + 1j*0.

            if fhigh:
                hpdata[int(fhigh/df):] = 0. + 1j*0.
                hcdata[int(fhigh/df):] = 0. + 1j*0.

        else:
            dt = 1./sample_rate
            hplus, hcross = ls.SimInspiralTD(phase, dt, m1, m2,
                                             s1x, s1y, s1z, s2x, s2y, s2z,
                                             flow, fref, dist, 0., iota,
                                             lambda1, lambda2, waveFlags, nonGRparams,
                                             amp_order, phase_order, approx)

            hpdata = hplus.data.data
            hcdata = hcross.data.data

            # Cut down to length if necessary
            ntime = int(duration * sample_rate)
            tc_index = int(round(-(hplus.epoch.gpsSeconds + 1e-9*hplus.epoch.gpsNanoSeconds)*sample_rate))
            if hpdata.shape[0] > ntime:
                tc_index -= hpdata.shape[0] - ntime
                hpdata = hpdata[-ntime:]
                hcdata = hcdata[-ntime:]

            # Now Fourier transiform; place the waveform's tC index
            # into the zero index of the FT array
            nbegin = hpdata.shape[0] - tc_index

            r2c_input_fft_array = np.zeros(ntime, dtype=np.float64)

            r2c_input_fft_array[:] = 0.0
            r2c_input_fft_array[:nbegin] = hpdata[tc_index:]
            r2c_input_fft_array[-tc_index:] = hpdata[:tc_index]
            hpdata = np.fft.rfft(r2c_input_fft_array)/sample_rate

            r2c_input_fft_array[:] = 0.0
            r2c_input_fft_array[:nbegin] = hcdata[tc_index:]
            r2c_input_fft_array[-tc_index:] = hcdata[:tc_index]
            hcdata = np.fft.rfft(r2c_input_fft_array)/sample_rate

    else:
        dt = 1./sample_rate
        hplus, hcross = ls.SimInspiralChooseTDWaveform(phase, dt, m1, m2,
                                                       s1x, s1y, s1z, s2x, s2y, s2z,
                                                       fmin, fref, dist, iota,
                                                       lambda1, lambda2, waveFlags, nonGRparams,
                                                       amp_order, phase_order, approx)

        hpdata = hplus.data.data
        hcdata = hcross.data.data

        # Cut down to length if necessary
        ntime = int(duration * sample_rate)
        tc_index = int(round(-(hplus.epoch.gpsSeconds + 1e-9*hplus.epoch.gpsNanoSeconds)*sample_rate))
        if hpdata.shape[0] > ntime:
            tc_index -= hpdata.shape[0] - ntime
            hpdata = hpdata[-ntime:]
            hcdata = hcdata[-ntime:]

        # Now Fourier transiform; place the waveform's tC index
        # into the zero index of the FT array
        nbegin = hpdata.shape[0] - tc_index

        r2c_input_fft_array = np.zeros(ntime, dtype=np.float64)

        r2c_input_fft_array[:] = 0.0
        r2c_input_fft_array[:nbegin] = hpdata[tc_index:]
        r2c_input_fft_array[-tc_index:] = hpdata[:tc_index]
        hpdata = np.fft.rfft(r2c_input_fft_array)/sample_rate

        r2c_input_fft_array[:] = 0.0
        r2c_input_fft_array[:nbegin] = hcdata[tc_index:]
        r2c_input_fft_array[-tc_index:] = hcdata[:tc_index]
        hcdata = np.fft.rfft(r2c_input_fft_array)/sample_rate

    return hpdata, hcdata


def project_on_network(hplus, hcross, time, ra, dec, psi=0., sample_rate=16384., duration=8., epoch=0., ifos=['H1', 'L1']):
    waveforms = []
    for ifo in ifos:
        gmst = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(time))

        diff = det_index(ifo)
        ifo_timedelay = timedelay(ifo, ra, dec, time)
        time_shift = time - epoch + ifo_timedelay
        fplus, fcross = lal.ComputeDetAMResponse(lal.CachedDetectors[diff].response, ra, dec, psi, gmst)

        hf = timeshift(fplus*hplus + fcross*hcross, time_shift, sample_rate, duration)
        waveforms.append(Strain(hf, delta_f=1./duration, epoch=epoch))

    return waveforms

def generate_strains(m1, m2, ra, dec, time=0., dist=1E6*ls.lal.PC_SI, iota=0., psi=0.,
                     s1x=0., s1y=0., s1z=0., s2x=0., s2y=0., s2z=0., phase=0.,
                     flow=30., fmin=30., fref=30., fhigh=None, lambda1=0., lambda2=0.,
                     amp_order=-1, phase_order=-1, waveFlags=None, nonGRparams=None,
                     approx=ls.IMRPhenomPv2, sample_rate=16384., duration=8., epoch=0.,
                     ifos=['H1', 'L1'], choose_fd=False):

    hp, hc = generate_hphc(m1, m2, dist, iota, s1x, s1y, s1z, s2x, s2y, s2z,
                           phase, fmin, flow, fref, fhigh, lambda1, lambda2,
                           waveFlags, nonGRparams, amp_order, phase_order, approx,
                           sample_rate, duration, choose_fd=choose_fd)

    return project_on_network(hp, hc, time, ra, dec, psi, sample_rate, duration, epoch, ifos)

def calibrate_strain(strain, spcal_logfreqs, spcal_amp, spcal_phase):
    dA = interpolate.spline(spcal_logfreqs, spcal_amp, np.log(strain.freqs))
    dPhi = interpolate.spline(spcal_logfreqs, spcal_phase, np.log(strain.freqs))

    cal_factor = (1.0 + dA) * (2.0 + 1j * dPhi) / (2.0 - 1j * dPhi)
    strain.data *= cal_factor
    return strain

def calibrate_strains_from_sample(strains, sample, ifos):
    try:
        sample_params = sample.keys()
    except AttributeError:
        sample = sample_to_waveform_params(sample)
        sample_params = sample.keys()

    cal_strains = []
    for ifo, strain in zip(ifos, strains):
        # Get frequencies of spline control points
        freq_params = sorted([param for param in sample_params if
                              '{}_spcal_freq'.format(ifo.lower()) in param])

        # Amplitude calibration model
        amp_params = sorted([param for param in sample_params if
                             '{0}_spcal_amp'.format(ifo.lower()) in param])

        # Phase calibration model
        phase_params = sorted([param for param in sample_params if
                               '{0}_spcal_phase'.format(ifo.lower()) in param])

        logfreqs = np.log([sample[param] for param in freq_params])
        spcal_amp = [sample[param] for param in amp_params]
        spcal_phase = [sample[param] for param in phase_params]

        cal_strains.append(calibrate_strain(strain, logfreqs, spcal_amp, spcal_phase))

    return cal_strains

def generate_strain_from_sample(sample, sample_rate=16384., duration=8., epoch=0., ifos=['H1', 'L1'],
                                flow=None, fmin=None, approx=None, choose_fd=False, calibrate=True, **kwargs):
    params = sample_to_waveform_params(sample)

    if flow is None:
        flow = params['flow']
        fmin = params['fmin']
    else:
        if fmin is None:
            fmin = flow_to_fmin(flow, params['amp_order'], params['approx'])

    if approx is None:
        approx = params['approx']

    if 'q1' in params.keys() and 'q2' in params.keys():
        non_gr_params = ls.SimInspiralCreateTestGRParam('q1', params['q1'])
        ls.SimInspiralAddTestGRParam(non_gr_params, 'q2', params['q2'])
    else:
        non_gr_params = None

    strains = generate_strains(params['m1'], params['m2'],
                               params['ra'], params['dec'],
                               params['time'], params['dist'],
                               params['iota'], params['psi'],
                               s1x=params['s1x'], s1y=params['s1y'], s1z=params['s1z'],
                               s2x=params['s2x'], s2y=params['s2y'], s2z=params['s2z'],
                               phase=params['phase'],
                               flow=flow, fmin=fmin, fref=params['fref'],
                               amp_order=params['amp_order'], phase_order=params['phase_order'],
                               approx=approx, nonGRparams=non_gr_params,
                               sample_rate=sample_rate, duration=duration, epoch=epoch, ifos=ifos,
                               choose_fd=choose_fd, **kwargs)

    if calibrate and params['spcal_active']:
        strains = calibrate_strains_from_sample(strains, params, ifos)

    return strains

def freq_evolution_from_sample(sample, sample_rate=16384., duration=8., epoch=0., flow=None, fmin=None, fhigh=None, ifos=['H1', 'L1'], ret_phase=False, **kwargs):
    params = sample_to_waveform_params(sample)

    approx = kwargs.get('approx', params['approx'])

    if flow is None:
        flow = params['flow']
        fmin = params['fmin']
    else:
        if fmin is None:
            fmin = flow_to_fmin(flow, params['amp_order'], approx)

    hplus, hcross = generate_hphc(params['m1'], params['m2'], params['dist'], params['iota'],
                                  params['s1x'], params['s1y'], params['s1z'],
                                  params['s2x'], params['s2y'], params['s2z'],
                                  params['phase'], fmin, flow, params['fref'], fhigh, 0., 0.,
                                  None, None, params['amp_order'], params['phase_order'], approx,
                                  sample_rate, duration, **kwargs)

    fts = []
    for ifo in ifos:
        ifo_timedelay = timedelay(ifo, params['ra'], params['dec'], params['time'])
        time_shift = params['time'] - epoch + ifo_timedelay

        hp = np.fft.irfft(timeshift(hplus.copy(), time_shift, sample_rate, duration)) * sample_rate
        hc = np.fft.irfft(timeshift(hcross.copy(), time_shift, sample_rate, duration)) * sample_rate

        # get amplitude evolution
        amp = np.sqrt(np.square(hp) + np.square(hc))

        # get frequency evolution using hplus, hcross and amplitude
        # because numpy.arctan2 doesn't take into account the number
        # of cycles, we have to correct for this manually
        sel = amp > 0.

        cos_h = np.zeros_like(amp)
        cos_h[sel] = hp[sel] / amp[sel]

        sin_h = np.zeros_like(amp)
        sin_h[sel] = hc[sel] / amp[sel]

        phase = np.unwrap(np.arctan2(sin_h, cos_h))
        if ret_phase:
            fts.append(phase)
        else:
            omega = np.concatenate([[0.], np.diff(phase)*sample_rate])
            fts.append(np.abs(omega)/(2*np.pi))
    return fts
