#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plotting routines.
"""

def specgrams(data, NFFT=1024, noverlap=None, ymin=None, ymax=None, axs=None, start_time=None, return_spec=False, **kwargs):
    from matplotlib import pyplot as plt
    ndata = len(data)

    if axs is None:
        fig, axs = plt.subplots(1, ndata, figsize=(6*ndata, 4), sharex=True, sharey=True)

    if noverlap is None:
        noverlap = NFFT/2

    cmap = kwargs.pop('cmap', 'viridis')

    vmin = kwargs.pop('vmin', None)
    vmax = kwargs.pop('vmax', None)

    Pxxs = []
    for ax, fd in zip(axs, data):
        time_low = start_time if start_time else fd.times[0]
        time_high = time_low + (fd.times[-1] - fd.times[0])
        Pxx, fs, bins, im = ax.specgram(fd.tseries, NFFT=NFFT,
                                        xextent=[time_low, time_high],
                                        Fs=fd.sample_rate, noverlap=noverlap,
                                        cmap=cmap, vmin=vmin, vmax=vmax, **kwargs)
        Pxxs.append(Pxx)
        ax.set_ylim(ymin=ymin, ymax=ymax)

    if return_spec:
        return axs, Pxxs
    else:
        return axs
