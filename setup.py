#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path
import re

try:
    from setuptools import setup

except ImportError:
    from distutils.core import setup

setup(
    name='gw-reconstruct',
    description='Assess and visualize gravitational wave signal reconstructions.',
    version = '0.1',
    author='Ben Farr',
    author_email='farr@uchicago.edu',
    url='https://git.ligo.org/bfarr/gw-reconstruction',
    include_package_data=True,
    packages=['gw_reconstruct'],
    install_requires=['numpy', 'scipy', 'matplotlib'],
)
